#!/bin/sh

if [ -z "${MATTERMOST_WEBHOOK}" ]; then
  echo "MATTERMOST_WEBHOOK is mandatory, please provide it!"
fi

if [ -z "${MATTERMOST_CHANNEL}" ]; then
  echo "MATTERMOST_CHANNEL is mandatory, please provide it!"
fi

if [ -z "${GARAGE_MATTERMOST_WEBHOOK}" ]; then
  echo "GARAGE_MATTERMOST_WEBHOOK is mandatory, please provide it!"
fi

if [ -z "${GARAGE_MATTERMOST_CHANNEL}" ]; then
  echo "GARAGE_MATTERMOST_CHANNEL is mandatory, please provide it!"
fi

# We use a busybox image, no way to use envsubst and Prometheus team had
# a long debate about whether env variable should be used for configuration,
# and they voted no. See https://github.com/prometheus/prometheus/issues/2357 for exemple.
# We have no other trivial way if we want to commit the configuration file without secrets inside.
mkdir -p /etc/amtool
cp /config/alertmanager.yml /etc/amtool/config.yml
sed -i "s@\$MATTERMOST_WEBHOOK@${MATTERMOST_WEBHOOK}@g" /etc/amtool/config.yml
sed -i "s@\$MATTERMOST_CHANNEL@${MATTERMOST_CHANNEL}@g" /etc/amtool/config.yml
sed -i "s@\$GARAGE_MATTERMOST_WEBHOOK@${GARAGE_MATTERMOST_WEBHOOK}@g" /etc/amtool/config.yml
sed -i "s@\$GARAGE_MATTERMOST_CHANNEL@${GARAGE_MATTERMOST_CHANNEL}@g" /etc/amtool/config.yml

# Substitue shell with alertmanager + arguments passed in Docker CMD
exec /bin/alertmanager $@

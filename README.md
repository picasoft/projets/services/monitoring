# Stack de monitoring

Ce dossier contient les ressources nécessaires pour déployer la partie serveur de la stack de monitoring de Picasoft, à savoir :

- `vmagent` pour l'ingestion de métriques
- Victoria Metrics pour le stockage de métriques
- BlackBox Exporter pour la vérification de la santé des services webs et des serveurs DNS
- `vmalert` pour la génération d'alertes
- AlertManager pour la gestion et la transmission des alertes
- Grafana pour visualiser les métriques

Pour des raisons de simplicités mais aussi de sécurité, ces services sont déployés sur la même machine, en partageant un même réseau Docker.

Ce README se veut le plus court possible : voir la [documentation associée à la pile de monitoring](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:start).

## Lancement

→ [Documentation générique](https://wiki.picasoft.net/doku.php?id=technique:docker:picasoft:admin#lancement_d_un_nouveau_service).

`exporters-auth.secrets` contient les identifiants pour les Basic Auth des pages `/metrics` des services. Voir [cette page](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:collect:service_metrics#configuration_traefik) pour savoir où les récupérer (normalement dans le pass, dossier `Tech/Prometheus-Exporters-Auth`).

Créer un fichier `.env` et renseigner les variables suivantes :

- `METRICS_AUTH` : chaîne d'authentification pour l'exporter Blackbox
- `ALERTMANAGER_AUTH` : chaîne d'authentification pour l'interface web d'`alertmanager`

Voir [cette page](https://wiki.picasoft.net/doku.php?id=technique:docker:good_practices:traefik#securiser_une_sous-partie_du_service_par_authentification) pour le format.

Ensuite, il ne reste plus qu'à construire les images qui le nécessitent et à tout lancer :

```bash
docker-compose build
docker-compose up -d
# On vérifie que tout marche bien
docker-compose logs -f
# Si tout a bien marché
docker-compose push
```

## Configuration

### Victoria Metrics

Victoria Metrics est la base de données orienté métriques (on parle de TSDB pour _time serie database_) [utilisée par Picasoft](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:victoriametrics).

Sa configuration est simple, et se fait entièrement à l'aide de paramètres en ligne de commande. Picasoft utilise les paramètres suivants :

- `-retentionPeriod=1200` : Configuration de la rétention des métriques pour 1200 mois, soit 100 ans, c'est à dire quasiment illimité
- `-storageDataPath=/victoria-metrics-data` : Dossier de stockage des métriques, ce dossier est monté dans un volume Docker pour persister les données
- `-selfScrapeInterval=10s` : Indique à Victoria Metrics d'auto-collecter des métriques sur son état de fonctionnement, toutes les 10 secondes
- `-selfScrapeInstance=vm-tsdb` : Indique le nom que l'on donne à cette instance de Victoria Metrics, qui sera inscrit en label sur les propres métriques de l'instance

### vmagent

`vmagent` est l'outil proposé par Victoria Metrics pour gérer l'ingestion de métriques, compatible avec plusieurs protocoles.

Sa principale utilisation à Picasoft est pour le scraping de métriques au format Prometheus. Pour cela `vmagent` prends en paramètre un fichier de configuration [identique à celui de Prometheus](https://prometheus.io/docs/prometheus/latest/configuration/configuration/), ici le fichier [`vmagent-prom.yml`](./vmagent-prom.yml), qui est monté dans le conteneur.  
Les valeurs dans ce fichiers peuvent-être substituées par des variables d'environnement en utilisant le format `%{ENV_VAR}`. C'est ce que l'on utilise pour les identifiants de certains exporters Prometheus de services. Pour cela on renseigne les valeurs dans le fichier [exporters-auth.secrets](./secrets/exporters-auth.secrets.example) à partir du contenu du Picapass.

Le reste de la configuration de `vmagent` se fait entièrement à l'aide de paramètres en ligne de commandes :

- `-promscrape.config=/config/vmagent-prom.yml` : Indique le chemin vers le fichier de configuration Prometheus.
- `-promscrape.config.strictParse` : Indique que l'on veut une interprétation stricte du fichier de configuration Prometheus (retourner une erreur en cas de format invalide ou de paramètres non supportés dans le fichier)
- `-remoteWrite.url=http://victoria-metrics:8428/api/v1/write` : Adresse vers laquelle `vmagent` doit envoyer les métriques collectées (l'instance Victoria Metrics)
- `-remoteWrite.tmpDataPath=/vmagent-remotewrite-data` : Dossier temporaire dans lequel `vmagent` stocke les métriques avant envoie vers Victoria Metrics (ou en cas d'indisponibilité de l'instance)

Pour une meilleure fiabilité, le dossier `/vmagent-remotewrite-data` qui stocke les données temporaires de `vmagent` est un volume Docker dédié : `vmagent-buffer`.

### Grafana

Grafana est l'outil de visualisation de métriques [utilisé par Picasoft](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:grafana).

#### Emplacements

La configuration est réalisée :

- Par les variables d'environnement de [Compose](./docker-compose.yml), qui permettent de configurer tous les paramètres (voir la [documentation](https://grafana.com/docs/grafana/latest/administration/configuration/#configure-with-environment-variables)) : cette méthode est utilisée pour tous les paramètres non-critiques.
- Par le fichier [ldap.toml](./ldap.toml) pour la configuration spécifique au LDAP. Ce fichier peut faire référence à des variables d'environnement avec [cette syntaxe](https://grafana.com/docs/grafana/latest/auth/ldap/#using-environment-variables), pour les données critiques (_e.g._ mot de passe)
- Par le fichier [grafana.secrets](./secrets/grafana.secrets.example), pour les secrets.

### Blackbox

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:collect:blackbox).

### vmalert

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:alerting:vmalert).

### alertmanager

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:alerting:alertmanager).

## Mise à jour

Pour Victoria Metrics et `vmagent` il suffit de changer les tags utilisés dans le fichier `docker-compose.yml`. On fera attention à utilise la même version pour les deux outils.

Pour Grafana il suffit aussi de changer le tag utilisé dans le fichier Docker Compose, on peut suivre les nouvelles version [sur la page des releases](https://github.com/grafana/grafana/releases).
